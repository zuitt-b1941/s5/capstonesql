-- 1.
SELECT * FROM customers WHERE country = "Philippines";

--2.
SELECT contactLastName, contactFirstName FROM customers where customerName = "La Rochelle Gifts";

--3.
SELECT productName, MSRP from products WHERE productName = "The Titanic";

--4.
SELECT firstName, lastName from employees WHERE email = "jfirrelli@classicmodelcars.com";

--5.
SELECT customerName FROM customers WHERE state IS NULL;

--6.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

--7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

--8.
SELECT orderNumber FROM orders WHERE comments LIKE "%DHL%";

--9.
SELECT productLine FROM productLines WHERE textDescription LIKE "%state of the art%";

--10.
SELECT DISTINCT country FROM customers;

--11.
SELECT DISTINCT status FROM orders;

--12.
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

--13. (NEED TO CORRECT)
SELECT firstName, lastName, city FROM employees
    JOIN offices ON employeeNumber = offices.employeeNumber.id;

--14. (NEED TO CORRECT)
SELECT customerName FROM customers 

--15. (NEED TO CORRECT)

--16.

--17.
SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;

--18.
SELECT customerName from customers WHERE phone LIKE "%+81%";
